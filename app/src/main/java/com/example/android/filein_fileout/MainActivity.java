package com.example.android.filein_fileout;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Xml;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xmlpull.v1.XmlSerializer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import static com.example.android.filein_fileout.R.id.myFile;


public class MainActivity extends AppCompatActivity {

    Button mybutt,myButt1;
    TextView myText;
    Boolean myBool;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        myButt1 = (Button)findViewById(R.id.button1);
        myButt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                viewFile();

            }
        });

        mybutt = (Button)findViewById(R.id.button);
        mybutt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                saveFile();

            }
        });



    }

    public void saveFile(){

        myBool = checkFile();

        if(myBool == false){

            Toast.makeText(getApplicationContext(), "FileFound", Toast.LENGTH_LONG).show();


        }

        else{

            Toast.makeText(getApplicationContext(), "File Being Created", Toast.LENGTH_LONG).show();
            try{

                FileOutputStream fOut = openFileOutput("current.xml", Context.MODE_APPEND);
                //OutputStreamWriter outputWriter = new OutputStreamWriter(fOut);
                XmlSerializer serializer = Xml.newSerializer();



                serializer.setOutput(fOut, "UTF-8");
                serializer.startDocument(null, Boolean.valueOf(true));
                //serializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);

                serializer.startTag(null, "records");
                serializer.startTag(null,"employee");
                serializer.startTag(null, "name");
                serializer.text("Ryan");
                serializer.endTag(null,"name");
                serializer.startTag(null,"surname");
                serializer.text("Derk");
                serializer.endTag(null,"surname");
                serializer.startTag(null,"salary");
                serializer.text("60000");
                serializer.endTag(null,"salary");
                serializer.endTag(null,"employee");
                serializer.endTag(null,"records");

                serializer.endDocument();
                serializer.flush();
                fOut.close();

                Toast.makeText(getApplicationContext(), "Save Successful", Toast.LENGTH_LONG).show();

            }
            catch(Throwable t){

                Toast.makeText(getApplicationContext(), "Save Unsuccessful", Toast.LENGTH_LONG).show();
                Toast.makeText(getApplicationContext(), "" + t.toString(), Toast.LENGTH_LONG).show();

            }

        }




    }

    public void viewFile(){

            try{

                myText = (TextView)findViewById(myFile);

                FileInputStream fileIn = openFileInput("current.xml");


                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                Document doc = dBuilder.parse(fileIn);

                Element element=doc.getDocumentElement();
                element.normalize();

                NodeList nList = doc.getElementsByTagName("employee");

                for (int i=0; i<nList.getLength(); i++) {

                    Node node = nList.item(i);
                    if (node.getNodeType() == Node.ELEMENT_NODE) {
                        Element element2 = (Element) node;
                        myText.setText(myText.getText()+"\nName : " + getValue("name", element2)+"\n");
                        myText.setText(myText.getText()+"Surname : " + getValue("surname", element2)+"\n");
                        myText.setText(myText.getText()+"-----------------------");
                    }
                }


            }
            catch(Throwable e) {

                Toast.makeText(getApplicationContext(), "" + e.toString(), Toast.LENGTH_LONG).show();

            }

    }

    private static String getValue(String tag, Element element) {
        NodeList nodeList = element.getElementsByTagName(tag).item(0).getChildNodes();
        Node node = nodeList.item(0);
        return node.getNodeValue();
    }

    public Boolean checkFile(){

        Boolean myBool2 = false;
        File file  = getBaseContext().getFileStreamPath("current.xml");

        if(file.exists()){

            Toast.makeText(getApplicationContext(), "FileFound", Toast.LENGTH_LONG).show();

        }

        else{

            Toast.makeText(getApplicationContext(), "No file found", Toast.LENGTH_LONG).show();
            myBool2 = true;
        }
            return myBool2;

    }
}
